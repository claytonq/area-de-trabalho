# Instalação do git no Debian GNU/Linux

```
sudo apt install git
```

Antes de começar configure algumas variáveis globais:
Após configurar estas opções você conseguirá visualizá-las com o comando `git config -l`
Elas também podem ser visualizadas através do arquivo `~/.gitconfig`

```
git config --global user.name "Nome Sobrenome"
git config --global user.email "email@dominio.net"
git config --global core.editor vim
git config --global color.ui true
```



# Operações locais básicas:

Inicia um desenvolvimento no diretória atual:
```
git init . 
```

Mostra a situação atual do diretório:
```
git status
```

Adiciona um arquivo do diretório atual para o controle de versão:
```
git add <arquivo>
```

Confirmar oficialmente ao controle de versão as novidades:
```
git commit <arquivos>

ou

git commit -a
```
-> Quando se dá um commit adiciona-se uma mensagem com um comentário ao trabalho realizado (80 colunas no máximo)

Facilita o trabalho remover e mover com apoio do git:
```
git rm
git mv
```

Mostra a ramificação (branch) onde estamos trabalhnado:
```
git branch
```

Sai de uma brach e vai para outra (cria uma nova branch):
```
git checkout -b desenvolvimento
git checout master
```

Junta as mudanças na brach em que estou trabalhando:
```
git merge desenvolvimento
```

Deleta uma brach
```
git branch -d desenvolvimento
```
-> alguns deixam as branchs como um histórico

Logs:
```
git log
git log --name-only 
```
-> mostra os nomes dos arquivos em cada log

Mostra todas as mudanças em um determinado arquivo:
```
git blame <arquivo>
```

Mostra a últimas modificações na tela, útil antes de um commit. Pode ser usado entre números de commits. Como números de logs é conveniênte informar os 3,5 bytes iniciais

```
git show zzzzzzz
``` 
-> Mostra o que foi feito nesse commit
```
git dif xxxxxxx yyyyyyy
``` 
-> Mostra o que aconteceu entre dois números de logs.
```
git dif yyyyyyy
``` 
-> Mostra as modificações realizadas a partir do número de log

Filtrando com git grep 
```
git rev-list --all
```
```
git grep B= $(git rev-list --all)
``` 
-> Verifica em todos os comits onde há o padrão B=

Reversão de commits feitos, trazer o que foi alterado de volta
```
git revert xxxxxxx
``` 
-> reverte e deixa descrito nos logs para todos verem

Reset Hard apaga tudo que foi feito à partir do commit informado
```
git reset --hard iiiiiii
```

Troca a última mensagem de log informada:
```
git commit --amend
```

Marcar com uma TAG o que foi feito (versão nova por exemplo)
```
git tag -a <tag> -m "Lançada a primeira versão"
```
-> tanto tags quanto commits podem ter informadas as mensagens com -m

Operações realizadas entre duas tags
```
git dif <tag1> <tag2> 
``` 




# Operações remotas  básicas:
Para se conectar com o servidor para poder efetuar push e pull, deve-se gerar uma chave ssh
```
ssh-keygen -t rsa -b 2048 -C "<comentario>"

```
-> Será solicitada uma passphrase.

Após gerar a chave ssh, poderá copiar seu conteúdo para a área de transferência, utilize o comando `cat ~/.ssh/id_rsa.pub`
Você precisará colar essas informações no campo adequado para suas chaves SSH nas preferências do seu perfil do GitLab.

Na interface web do gitlab crie um novo projeto vazio. Certifique de já ter realizado as configurações globais já citdas.

Para clonar o projeto para a máquina local:
```
git clone git@gitlab.com:nome/nome-do-projeto.git
```

Criando um novo repositório:
```
git clone git@gitlab.com:seunome/nome-do-repositorio.git
cd nome-do-repositorio
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main
```

Enviando uma pasta já existente:
```
cd existing_folder
git init --initial-branch=main
git remote add origin git@gitlab.com:seunome/nome-do-repositorio.git
git add .
git commit -m "Initial commit"
git push -u origin main
```

Baixando um repoistorio existente:
```
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:seunome/nome-do-repositorio.git
git push -u origin --all
git push -u origin --tags
```

Enviando as alterações feitas para o repositório:
```
git push --all
```
```
git push --tags
```
-> Copiar a linha fornecida pelo repositório com endereço e identificação do usuário.


Busca alterações de um repositório remoto já configurado localmente:
```
git pull
```


 
