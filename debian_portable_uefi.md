# Instalação de GNU/Linux Debian Bookworm em mídia removivel criptografada - sistema operacional portável e secreto

Aqui veremos como foi realizada uma instalação do sistema Debian em um dispositivo de mídia removivel USB, neste caso um pendrive com 32GB de capacidade. Assim ele poderá ser usado para dar partida (boot) em qualquer computador que tenhamos permissão. Um sistema "operacional de bolso".

Todo a preparação desta nova instalação se deu partir de um sistema GNU/Linux Debian Bullseye em produção (host). Todos os dados importantes contidos no pendrive usado foram previamente salvas pois após a realização desse procedimento toda informação contida nele previamente foi perdida.

# Preparo do disco removível
Foi preciso conectar o pendrive na máquina par iniciar o procedimento, porém a forma mais segura de inicar um procedimento assim é fazendo uma busca sobre quais os dispositivos de disco que já estavam sendo reconhecidos pelo sitema em produção. Portanto antes mesmo de conectar o pendrive que utilizei usei o comando `sudo lsblk -p`.
```
lsblk -p
NAME        MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
/dev/sda      8:0    0 931,5G  0 disk 
├─/dev/sda1   8:1    0   600M  0 part /boot/efi
├─/dev/sda2   8:2    0 153,7G  0 part /
├─/dev/sda3   8:3    0  14,9G  0 part [SWAP]
└─/dev/sda4   8:4    0 762,3G  0 part /home
```
A opção `-p` deste comando nos mostra o caminho completo em que os dispositivos de discos estão endereçados no sistema. Assim foi possível identificar o dispositivo de disco em `/dev/sda` que contém as partições em `/dev/sda1`, `/dev/sda2` e `/dev/sda4` usando os respectivos diretórios como pontos de montagem: `/boot/efi`, `/` e `/home`. A partição em `/dev/sda3` também é mostrada porém não está montada em nenhum ponto pois é uma partição do tipo `[SWAP]` que é utilizada de outra forma pelo sistema e não precisa ser montada.


Em seguida conectei o pendrive na máquina e usei o mesmo comando, assim fica mais fácil descobrir qual é o novo dispositivo de disco que o sistema acaba de reconhecer:
```
lsblk -p
NAME        MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
/dev/sda      8:0    0 931,5G  0 disk 
├─/dev/sda1   8:1    0   600M  0 part /boot/efi
├─/dev/sda2   8:2    0 153,7G  0 part /
├─/dev/sda3   8:3    0  14,9G  0 part [SWAP]
└─/dev/sda4   8:4    0 762,3G  0 part /home
/dev/sdb      8:16   1  31,3G  0 disk 
└─/dev/sdb1   8:17   1  31,3G  0 part /media/user/1659AAC714B1576C
```
Neste caso o novo dispositivo reconhecido disco fica em `/dev/sdb` que contém a partição em `/dev/sdb1` que o sistema montou automaticamente no diretório `/media/usuario/1659AAC714B1576C`

Lembre-se que é crucial saber exatamente qual o endreço do dispositivo no sistema antes de dar andamento a esta preparação para a nova instalação. Visto que realizaremos operações que gravarão mudanças permanentes no dispositivo de disco que vamos prepar, qualquer confusão com relação ao dispositivo que realizaremos as operações seguintes podem causar danos irreversíveis ao sistema que estamos usando para realizar esta tarefa. Portanto deve se tomar a devida atenção antes de aplicar qualquer comando descrito a seguir.


## Particionar a mídia removível:
Para o novo sistema que será instalado no pendrive defini apenas 2 partições para o pendrive. A primeira é uma partição de 50MB que é ncessária para o sistema de boot UEFI e a segunda partição ocupa todo o restante do disco. Ou seja, uma única partição onde o novo sistema operacional é instalado.
```
sudo fdisk /dev/sdb
g ->	cria uma tabela para particionamento do tipo GPT vazia
n ->	cria uma nova partição
 1	->	define que será a primeira partição
 2048	->	assume que esse será o primeiro setor para esta partição
 +50M	->	define o tamanho para essa partição [50Mb]
 t	->	altera o tipo da partição
 1	->	define o tipo EFI - ESP

n ->	cria uma nova partição
 2	->	define que erá a segunda partição
 104448	->	assumen que esse será o primeiro setor para esta partição
 65535966->	define o tamanho para essa partição [todo o restante do disco]

w ->	grava todas as alterações realizadas e sai
```

## Vizualisando o particionamento criado:
```
Criada uma nova partição 2 do tipo "Linux filesystem" e de tamanho 31,2 GiB.

Comando (m para ajuda): p
Disco /dev/sdb: 31,26 GiB, 33554432000 bytes, 65536000 setores
Disk model: Flash Disk      
Unidades: setor de 1 * 512 = 512 bytes
Tamanho de setor (lógico/físico): 512 bytes / 512 bytes
Tamanho E/S (mínimo/ótimo): 512 bytes / 512 bytes
Tipo de rótulo do disco: gpt
Identificador do disco: 4B13E53E-1B22-AC4B-9C4B-CE92775C1319

Dispositivo Início      Fim  Setores Tamanho Tipo
/dev/sdb1     2048   104447   102400     50M Sistema EFI
/dev/sdb2   104448 65535966 65431519   31,2G Linux sistema de arquivos

Assinatura de sistema de arquivos/RAID na partição 1 será apagada.
```

### Criando Luks
Esta é a ferramenta utilizada para criptografar a partição do pendrive antes da instalação do sistema operacional. Nesta etapa definimos uma frase secreta que é necessária para acessar os dados desta partição. Somente quem tem acesso a esta frase pode acessar os dados da partição do pendrive e consequentemente usar o sistema instalado nele.

Instalando a aplicação no host:
```
apt install cryptsetup
```

Preparando a partição:
É necessário confirmar com um YES em maiúsculo para realizar a formatação com luks1.
É necessário definir uma frase secreta para ter acesso à partição criptografada no pendrive.

```
sudo cryptsetup luksFormat --type luks1 /dev/sdb2

WARNING!
========
Isto irá sobrescrever os arquivos em /dev/sdb2 definitivamente.

Are you sure? (Type uppercase yes): YES
Informe a frase secreta para /dev/sdb2: 
Verificar frase secreta:
```
Com o comando `sudo cryptsetup luksDump` é possível verificar se novas informações foram gravadas na partição criptografada:
```
sudo cryptsetup luksDump /dev/sdb2
LUKS header information for /dev/sdb2

Version:       	1
Cipher name:   	aes
Cipher mode:   	xts-plain64
Hash spec:     	sha256
Payload offset:	4096
MK bits:       	512
MK digest:     	8c 6e 6b 94 47 64 63 07 b0 38 12 d3 ee 8f 0c e2 43 51 20 d5 
MK salt:       	b8 75 9d 8b 57 18 af 9e 1d e6 1f 13 49 1d a7 3a 
               	51 7b 97 ce 97 19 ca df 87 46 4f ea cf 74 93 7a 
MK iterations: 	109959
UUID:          	c6d61f13-a405-4f7a-98cf-a35bc03170ea

Key Slot 0: ENABLED
	Iterations:         	1798586
	Salt:               	9a 65 8e 79 bf df b0 08 5f fa 4c b6 4b c3 b7 d6 
	                      	58 8a 59 6f f9 f7 b8 11 40 cc 9d 4a dc 58 55 aa 
	Key material offset:	8
	AF stripes:            	4000
Key Slot 1: DISABLED
Key Slot 2: DISABLED
Key Slot 3: DISABLED
Key Slot 4: DISABLED
Key Slot 5: DISABLED
Key Slot 6: DISABLED
Key Slot 7: DISABLED
```
Analizando as informações conseguimos perceber que a frase secreta utilizada foi armazenada no Key Slot 0. Usando luks1 podemos armazenar frases secretas em até 8 slots diferentes, isso é útil como veremos posteriormente.

## Criando um mapeamento para a partição luks
Para ter acesso à partição que preparei:
```
sudo cryptsetup luksOpen /dev/sdb2 criptografado  
Informe a frase secreta para /dev/sdb2: 
```
Usamos para isso a frase secreta que criamos anteriormente.

Posso ver o mapeamento criado com o comando `lsblk`, o sistema host reconheceu um novo dispositivo em `/dev/mapper/criptografado`. Poderia ter sido definido outro nome diferente de `criptografado` quando usei o comando anterior.
```
lsblk -p /dev/sdb
NAME                          MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINT
/dev/sdb                        8:16   1  31,3G  0 disk  
├─/dev/sdb1                     8:17   1    50M  0 part  
└─/dev/sdb2                     8:18   1  31,2G  0 part  
  └─/dev/mapper/criptografado 253:0    0  31,2G  0 crypt
```

### Formatando as partições do disco removível:
Para `dev/sdb1` a formatação com o tipo de arquivo `vfat` é necessária pois este é o tipo de sistemas de arquivos reconhecido pela tecnologia UEFI das máquinas atuais.
```
sudo mkfs.vfat /dev/sdb1
mkfs.fat 4.1 (2017-01-24)
```

Já a aprtição em `/dev/mapper/criptografado` onde o novo sistema operacional é instalado, posso usar um sistema de arquivos diferente optei por usar `ext4`.
Faço isso com o comando `sudo mkfs.ext4` que demorou um pouco mais para concluir:
```
sudo mkfs.ext4 /dev/mapper/criptografado 
mke2fs 1.45.5 (07-Jan-2020)
A criar sistema de ficheiros com 8178427 4k blocos e 2048000 inodes
UUID do sistema de ficheiros: f24c3fdc-b9c2-444d-9270-7ac6a6f7092b
Cópias de segurança de superblocos gravadas em blocos: 
	32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632, 2654208, 
	4096000, 7962624

A alocar tabelas de grupo: pronto                            
Gravando tabelas inode: pronto                            
A criar diário (32768 blocos): concluído
Escrevendo superblocos e informações de contabilidade de sistema de arquivos: concluído
```

# Instalar o Debian GNU/Linux
Usei o `grml-debootstrap`, para automatizar a nova instalação, para isso o instalei no host com:
```
sudo apt install grml-debootstrap
```

Realizar a instalação do Debian GNU/Linux Bookworm no pendrive:
```
sudo grml-debootstrap --release bookworm --efi /dev/sdb1 --target /dev/mapper/criptografado --grub /dev/sdb --hostname debianusb --password senhadoroot
```
Observe que nesse momento consigo informar o hostname para o sistema que instalo no pendrive assim como a senha para o usuário root após as opções `--hostname` e `--password` respectivamente. Fiz isso de acordo com minhas preferências e tomei nota.

Confirmo a operação com `y` e `[ENTER]`
```
 * EFI support enabled now.
 * grml-debootstrap [0.90] - Please recheck configuration before execution:

   Target:          /dev/mapper/criptografado
   Install grub:    /dev/sdb
   Install efi:     /dev/sdb1
   Using release:   bookworm
   Using hostname:  debianusb
   Using mirror:    http://deb.debian.org/debian
   Using arch:      amd64
   Config files:    /etc/debootstrap

   Important! Continuing will delete all data from /dev/mapper/criptografado!

 * Is this ok for you? [y/N] ## Outras configurações adicionais
```

Surgiram algumas mensagens pedindo confirmações posteriormente.`y` e `[ENTER]` novamenta:
```
* EFI partition /dev/sdb1 seems to have a FAT filesystem, not modifying.
 * Running mkfs.ext4  on /dev/mapper/criptografado
mke2fs 1.45.5 (07-Jan-2020)
/dev/mapper/criptografado contains a ext4 file system
	created on Mon Nov  1 17:40:36 2021
Proceed anyway? (y,N) y
```
Aguardei até o processo terminar, essa parte demorou um pouco...

## Instalar pacotes extras
Terminada a instalação do Debian realizei o procedimento de `chroot` para efetuar algumas mudanças para que o sistema desse boot usando uma partição criptografada, e também instalei alguns pacotes adicionais para o novo sistema no pendrive.

Preparando o ambiente antes do chroot:
As partições do pendrive removível e também alguns diretórios do sistema host precisam ser montadas para isso:
```
sudo mount /dev/mapper/criptografado /mnt/
sudo mount /dev/sdb1 /mnt/boot/efi/
sudo mount --bind /proc /mnt/proc
sudo mount --bind /dev /mnt/dev
sudo mount --bind /sys /mnt/sys
```
Feito o procedimento ácima posso fazer a transição do sistema em nosso host para desse momento em diante operar diretamente no novo sistema que foi instalao no pendrive:
```
sudo chroot /mnt
```

E para ter certeza que o sistema operacional é realmente o novo sistema instalado no pendrive:
```
cat /etc/hostname 
debianusb
```
Observer que o nome apresentado em `/etc/hostname` é exatamente o mesmo definido no momento em que usamos o comando `grml-bootstrap` para instalar o Debian Bookworm no pendrive. Portanto tendo certeza disso, dei andamento:

Preparando initrd para poder abrir luks
```
apt install cryptsetup-initramfs
cp /usr/share/initramfs-tools/hooks/cryptkeyctl /etc/initramfs-tools/hooks
```
Neste momento o programa console-setup pedirá para definir o mapa de teclado, faça de acordo com seu laoyt atual. No meu caso Portugues, Brasil, Nativo.


Atualizando os UIDs:
```
ls -l /dev/disk/by-uuid/
total 0
lrwxrwxrwx 1 root root 10 nov  1 21:42 1376-17E6 -> ../../sdb1
lrwxrwxrwx 1 root root 10 nov  1 16:38 379d198e-81ec-4dfb-9356-c568043ac436 -> ../../sda4
lrwxrwxrwx 1 root root 10 nov  1 16:38 3CF5-8254 -> ../../sda1
lrwxrwxrwx 1 root root 10 nov  1 21:59 618633f0-0766-4e31-8388-aff462a772b2 -> ../../dm-0
lrwxrwxrwx 1 root root 10 nov  1 16:38 8ac3f7ba-0de9-4514-8706-43f7eb48042a -> ../../sda3
lrwxrwxrwx 1 root root 10 nov  1 21:34 c6d61f13-a405-4f7a-98cf-a35bc03170ea -> ../../sdb2
lrwxrwxrwx 1 root root 10 nov  1 16:38 e415af66-8d8a-4b5e-85b5-c7e65f2e3264 -> ../../sda2
```

```
cat /etc/crypttab 
# <target name>	<source device>		<key file>	<options>
criptografado UUID=c6d61f13-a405-4f7a-98cf-a35bc03170ea none luks,discard
```
Veja que no crypttab o UUID da partição deve corresponder à partição em que o volume `criptografado` foi criada e não o UUID do volume `criptografado`

Preparando novo fstab
```
apt install arch-install-scripts
genfstab  / >/etc/fstab
```
Aqui o arquivo `/etc/fstab` foi gerado e inclui a partição `sda3` [SWAP] que o sistema atual encherga. Porém editei o arquivo e retirei a linha referente a esta partição.

Editando grub (/etc/default/grub)
```
GRUB_CMDLINE_LINUX="root=/dev/mapper/criptografado cryptdevice=UUID=c6d61f13-a405-4f7a-98cf-a35bc03170ea:criptografado"
GRUB_ENABLE_CRYPTODISK=y
```
Instalando o grub em `/dev/sdb`
```
grub-install /dev/sdb
```

Criando uma chave
** Para não ter que digitar a senha 2x, uma para o grub outra para o kernel**
```
dd bs=512 count=4 if=/dev/urandom of=/crypto_keyfile.bin
chmod 000 /crypto_keyfile.bin
```

```
cryptsetup luksAddKey /dev/sdb2 /crypto_keyfile.bin 
Digite qualquer senha existente:
```
Aqui, para adicionar uma nova chave a um novo slot do luks é necessário usar a primeira frase secreta que definimos anteriormente.

## Para que nenhum usuário local sem ser root possa ler o initrd e copiar a chave
Incluir no arquivo `/etc/initramfs-tools/initramfs.conf`
```
UMASK=0077
```

Para incluir chave no initramfs
  - no arquivo /etc/cryptsetup-initramfs/conf-hook
  - definir as variáveis dessa forma:
```
CRYPTSETUP=y
KEYFILE_PATTERN=/*.bin
```

Adicione ao `/etc/crypttab` a opção para procurar a chave em `/crypto_keyfile.bin`
```
cat /etc/crypttab 
# <target name>	<source device>		<key file>	<options>
criptografado UUID=c6d61f13-a405-4f7a-98cf-a35bc03170ea /crypto_keyfile.bin luks,discard
```


Agora atualizamos o initramfs
```
update-initramfs -u -k all
```

Criando arquivo de configuração do GRUB
Para não executar os_prober
```
chmod -x /etc/grub.d/30_os-prober
update-grub
```

## Outras configurações adicionais
Configurar o locales:
```
dpkg-reconfigure locales
```
Será exibida a tela onde podemos escolher o padrão `pt_BR.UTF-8 UTF-8 ` para ser gerado. Na tela seguinte poss também definir esse padrão gerado como o locale predefinido para o sistema. Selecione a esta opção e `[OK]`

Criei um usuário chamado `usuario` e informei uma senha:
```
adduser usuario
Adicionando usuário 'usuario' ...
Adicionando novo grupo 'usuario' (1000) ...
Adicionando novo usuário 'usuario' (1000) com grupo 'usuario' ...
Criando diretório pessoal '/home/usuario' ...
Copiando arquivos de '/etc/skel' ...
Nova senha:
Redigite a nova senha:
passwd: senha atualizada com sucesso
Modificando as informações de usuário para usuario
Informe o novo valor ou pressione ENTER para aceitar o padrão
	Nome Completo []:
	Número da Sala []:
	Fone de Trabalho []:
	Fone Residencial []:
	Outro []:
A informação está correta? [S/n]
```

Colocar o usuario no grupo sudo:
```
adduser usuario sudo
Adicionando usuário 'usuario' ao grupo 'sudo' ...
Adicionando usuário usuario ao grupo sudo
Concluído.
```

Instalar o sudo:
```
apt install sudo
```


Instalar outros pacotes importantes:
```
apt install bash-completion network-manager shim-signed
```


Saindo do chroot e desmontando tudo
```
exit
```
```
sudo umount /mnt/sys
sudo umount /mnt/proc
sudo umount /mnt/dev
sudo umount /mnt/boot/efi
sudo umount /mnt
sudo cryptsetup luksClose criptografado
```
